package labactivity11;

public class Circle implements Shape {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.pow(this.radius, 2) * Math.PI;
    }

    public double getPerimeter() {
        return (this.radius * 2) * Math.PI;
    }

    public double getRadius() {
        return radius;
    }

}

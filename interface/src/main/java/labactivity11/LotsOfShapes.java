package labactivity11;

public class LotsOfShapes {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[5];
        shapes[0] = new Rectangle(1, 2);
        shapes[1] = new Circle(7);
        shapes[2] = new Rectangle(4, 8.5);
        shapes[3] = new Circle(7.3);
        shapes[4] = new Square(5);
        for (Shape shape : shapes) {
            System.out.println("The area: " + shape.getArea());
            System.out.println("The perimeter: " + shape.getPerimeter());
        }
    }
}
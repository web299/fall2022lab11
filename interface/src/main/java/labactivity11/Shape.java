package labactivity11;

public interface Shape {
    double getArea();

    double getPerimeter();
}
package labactivity11;

public class Rectangle implements Shape {
    private double height;
    private double width;

    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public double getArea() {
        return this.width * this.height;
    }

    @Override
    public double getPerimeter() {
        return (this.height + this.width) * 2;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

}
